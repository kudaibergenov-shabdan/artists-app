const mongoose = require('mongoose');

const AlbumSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: true,
    },
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artist',
        required: true,
        unique: true,
    },
    issueYear: {
        type: Number,
        min: [1900, 'issueYear cannot be smaller than 1900'],
        max: [2999, 'issueYear cannot be bigger than 2999'],
    },
    image: String,
});

const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;