const mongoose = require('mongoose');

const TrackSchema = new mongoose.Schema({
    track: {
        type: String,
        required: true,
        unique: true,
    },
    trackNumber: {
        type: Number,
        validate: [
            {
                validator: async function(value) {
                    if (value) {
                        const foundTrackNumber = await Track.findOne({
                            album: this.album,
                            trackNumber: value,
                        });
                        if (foundTrackNumber)
                            return false;
                    }
                },
                message: 'Such number is already existed'
            },
            {
                validator: async value => {
                    if (value) {
                        return value > 0;
                    }
                },
                message: 'trackNumber cannot be less than 0'
            },
        ],
    },
    album: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Album',
    },
    duration: String,
});

const Track = mongoose.model('Track', TrackSchema);
module.exports = Track;