const express = require('express');
const router = express.Router();
const Track = require('../models/Track');
const Album = require("../models/Album");
const permit = require("../middleware/permit");
const auth = require("../middleware/auth");

router.get('/', async (req, res) => {
    const query = {};

    if (req.query.album) {
        query.album = req.query.album;
    }

    try {
        const tracks = await Track.find(query).populate(
            {
                path: 'album',
                select: 'title',
                populate: {path: 'artist', select: 'title'}
            }
        ).sort({trackNumber: 1});
        res.send(tracks);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const album = await Album.findById(req.params.id).populate('artist', 'title');

        if (!album) {
            res.status(404).send({error: 'Album not found'});
        } else {
            const tracks = await Track.aggregate([
                {$match: {album: album._id}},
                {$sort: {trackNumber: 1}}
            ]);

            if (!tracks) {
                return res.status(400).send({error: 'No tracks found'});
            }
            res.send(tracks);
        }
    } catch (error) {
        res.send(error);
    }
});

router.post('/', auth, permit('admin', 'user'), async (req, res) => {
    if (!req.body.track) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const trackData = {
        track: req.body.track,
        trackNumber: req.body.trackNumber || null,
        album: req.body.album || null,
        duration: req.body.duration || null,
    };

    const track = new Track(trackData);
    try {
        await track.save();
        res.send(track);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;