const express = require('express');
const router = express.Router();
const Artist = require('../models/Artist');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const path = require('path');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', auth, async (req, res) => {
    try {
        const artists = await Artist.find();
        res.send(artists);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', auth, permit('admin', 'user'), upload.single('image'), async (req, res) => {
    try {
        const artistData = {
            title: req.body.title,
            info: req.body.info || null
        };

        if (req.file) {
            artistData.image = 'uploads/' + req.file.filename;
        }

        const artist = new Artist(artistData);

        await artist.save();
        res.send(artist);
    } catch (error) {
        res.status(400).send(error);
    }
});


module.exports = router;