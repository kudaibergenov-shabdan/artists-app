const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const auth = require("../middleware/auth");
const router = express.Router();

router.post('/', auth, async (req, res) => {
    if (!req.body.track) {
        return res.status(400).send({error: 'You have not put track'});
    }

    const trackHistoryData = {
        user: req.user,
        track: req.body.track,
        datetime: new Date(),
    };

    const trackHistory = new TrackHistory(trackHistoryData);
    try {
        await trackHistory.save();
        res.send(trackHistory);
    }
    catch (e) {
        res.status(400).send({error: 'Data not valid.'})
    }
});

router.get ('/', auth, async (req,res) => {
    const query = {
        user: req.user
    };
    const trackHistory = await TrackHistory.find(query).populate(
        {
            path: 'track',
            select: 'track',
            populate: {
                path: 'album',
                select: 'title',
                populate: {
                    path: 'artist',
                    select: 'title',
                },
            },
        },
    ).sort({datetime: -1});
    if (!trackHistory) {
        return res.status(400).send({message: 'Nothing were found'});
    }
    res.send(trackHistory);
});

module.exports = router;