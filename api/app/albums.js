const express = require('express');
const router = express.Router();
const Album = require('../models/Album');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const path = require('path');
const Artist = require("../models/Artist");
const permit = require("../middleware/permit");
const auth = require("../middleware/auth");
const {CastError} = require('mongoose').Error;

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    const query = {};

    if (req.query.artist) {
        query.artist = req.query.artist;
    }

    try {
        const albums = await Album.find(query).populate('artist');
        res.send(albums);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const user = await Artist.findById(req.params.id);
        if (!user) {
            return res.status(400).send({error: 'Incorrect artist id entered.'});
        }

        const albums = await Album.aggregate([
            {$match: {artist: user._id}},
            {$sort: {issueYear: 1}},
            {
                $lookup: {
                    "from": Artist.collection.name,
                    "localField": "artist",
                    "foreignField": "_id",
                    "as": "artistInfo"
                }
            },
        ]);

        if (!albums) {
            return res.status(400).send({error: 'No albums found'});
        }
        res.send(albums);
    } catch (error) {
        res.send(error);
    }
});

router.post('/', auth, permit('admin', 'user'), upload.single('image'), async (req, res) => {
    try {
        const albumData = {
            title: req.body.title,
            artist: req.body.artist,
            issueYear: req.body.issueYear
        };

        if (req.file) {
            albumData.image = 'uploads/' + req.file.filename;
        }

        const album = new Album(albumData);

        await album.save();

        res.send(album);
    } catch (error) {
        if (error.errors.issueYear.name instanceof CastError) {
            error.errors.issueYear.message = `Value ${req.body.issueYear} is not a valid number!`;
        }
        res.status(400).send(error);
    }
})

module.exports = router;