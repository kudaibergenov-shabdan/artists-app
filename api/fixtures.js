const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Artist = require("./models/Artist");
const Album = require("./models/Album");
const Track = require("./models/Track");
const User = require("./models/User");
const TrackHistory = require("./models/TrackHistory");


const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();
    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [artistLimpBizkit, artistFiftyCent] = await Artist.create(
        {
            title: 'Limp Bizkit',
            info: 'Rock group',
            image: 'fixtures/limpbizkit.jpg',
        },
        {
            title: '50 Cent',
            info: 'Rap star',
            image: 'fixtures/50cent.jpeg',
        },
    );

    const [limpBizkitAlbum, fiftyCentAlbum] = await Album.create(
        {
            title: 'Chocolate Starfish and the Hot Dog Flavored Water',
            artist: artistLimpBizkit,
            issueYear: 2000,
            image: 'fixtures/chocolate_starfish.png'
        },
        {
            title: `Get Rich or Die Tryin'`,
            artist: artistFiftyCent,
            issueYear: 2003,
            image: 'fixtures/get_rich.jpg',
        },
    );

    const [trackLimpBizkit, trackFiftyCent] = await Track.create(
        {
            track: 'My Way',
            trackNumber: 5,
            album: limpBizkitAlbum,
        },
        {
            track: 'In da Club',
            trackNumber: 5,
            album: fiftyCentAlbum,
            duration: '4 min',
        },
    );

    const [adminUser, regularUser] = await User.create(
        {
            username: 'admin',
            password: '123',
            token: nanoid(),
            role: 'admin',
        },
        {
            username: 'user',
            password: '123',
            token: nanoid(),
            role: 'user',
        }
    );

    await TrackHistory.create(
        {
            user: adminUser,
            track: trackLimpBizkit,
            datetime: new Date(),
        },
        {
            user: adminUser,
            track: trackFiftyCent,
            datetime: new Date(),
        },
        {
            user: adminUser,
            track: trackLimpBizkit,
            datetime: new Date(),
        },
        {
            user: regularUser,
            track: trackFiftyCent,
            datetime: new Date(),
        },
        {
            user: regularUser,
            track: trackLimpBizkit,
            datetime: new Date(),
        },
    );

    await mongoose.connection.close();
};

run().catch(console.error);