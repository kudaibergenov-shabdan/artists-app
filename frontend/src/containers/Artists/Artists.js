import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsActions";
import {Button, CircularProgress, Grid, Typography} from "@material-ui/core";
import ArtistItem from "../../components/ArtistItem/ArtistItem";
import {Link, Redirect} from "react-router-dom";

const Artists = () => {
  const dispatch = useDispatch();
  const artists = useSelector(state => state.artists.artists);
  const fetchingLoading = useSelector(state => state.artists.fetchingLoading);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    dispatch(fetchArtists());
  }, [dispatch]);

  if (!user) {
    return <Redirect to="/login"/>
  }

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item container justifyContent="space-between" alignItems="center">
        <Grid item>
          <Typography variant="h4">Artists</Typography>
        </Grid>

        {(user?.role === 'admin' || user?.role === 'user') && (
          <Grid item>
            <Grid container spacing={2}>
              <Grid item>
                <Button color="primary" variant="contained" component={Link} to="/artist/new">Add Artist</Button>
              </Grid>
              <Grid item>
                <Button color="inherit" variant="contained" component={Link} to="/album/new">Add Album</Button>
              </Grid>
              <Grid item>
                <Button color="secondary" variant="contained" component={Link} to="/track/new">Add Track</Button>
              </Grid>
            </Grid>
          </Grid>
        )}
      </Grid>

      <Grid item>
        <Grid item container direction="row" spacing={1}>
          {fetchingLoading
            ?
            <Grid container justifyContent="center" alignItems="center">
              <Grid item>
                <CircularProgress/>
              </Grid>
            </Grid>
            :
            artists.map(artist => (
              <ArtistItem
                key={artist._id}
                id={artist._id}
                name={artist.title}
                image={artist.image}
              />
            ))
          }
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Artists;