import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchTracks} from "../../store/actions/tracksActions";
import {CircularProgress, Grid} from "@material-ui/core";
import TrackItem from "../../components/TrackItem/TrackItem";

const Tracks = ({location}) => {
  const dispatch = useDispatch();
  const tracks = useSelector(state => state.tracks.tracks);
  const fetchingLoading = useSelector(state => state.tracks.fetchingLoading);

  useEffect(() => {
    dispatch(fetchTracks(location.search));
  }, [dispatch, location.search]);

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item>
        <Grid item container direction="row" spacing={1}>
          {fetchingLoading
            ?
            <Grid container justifyContent="center" alignItems="center">
              <Grid item>
                <CircularProgress/>
              </Grid>
            </Grid>
            :
            <>
              {
                (tracks[0])
                &&
                <Grid container direction="column" spacing={2}>
                  <Grid item>
                    {tracks[0].album.artist.title} - {tracks[0].album.title}
                  </Grid>
                </Grid>
              }
              {tracks.map(track => (
                <TrackItem key={track._id} track={track}/>
              ))}
            </>
          }
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Tracks;