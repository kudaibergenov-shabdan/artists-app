import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchTracksHistory} from "../../store/actions/trackHistoryActions";
import {CircularProgress, Grid} from "@material-ui/core";
import {Redirect} from "react-router-dom";
import TrackHistoryItem from "../../components/TrackHistoryItem/TrackHistoryItem";

const TrackHistory = () => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const trackHistory = useSelector(state => state.trackHistoryReducer.trackHistory);
  const fetchingLoading = useSelector(state => state.trackHistoryReducer.fetchingLoading);

  useEffect(() => {
    dispatch(fetchTracksHistory());
  }, [dispatch]);

  if (!user) {
    return <Redirect to="/login"/>
  }

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item>
        <Grid item container direction="row" spacing={1}>
          {
            fetchingLoading ? (
                <Grid container justifyContent="center" alignItems="center">
                  <Grid item>
                    <CircularProgress/>
                  </Grid>
                </Grid>
              ) :
              trackHistory.map(histTrack => (
                <TrackHistoryItem key={histTrack._id} histTrack={histTrack}/>
              ))
          }
        </Grid>
      </Grid>
    </Grid>
  );
};

export default TrackHistory;