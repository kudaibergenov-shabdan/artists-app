import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchAlbums} from "../../store/actions/albumsActions";
import {CircularProgress, Grid} from "@material-ui/core";
import AlbumItem from "../../components/AlbumItem/AlbumItem";

const Albums = ({match}) => {
  const dispatch = useDispatch();
  const albums = useSelector(state => state.albums.albums);
  const fetchingLoading = useSelector(state => state.albums.fetchingLoading);

  useEffect(() => {
    dispatch(fetchAlbums(match.params.id));
  }, [dispatch, match.params.id]);

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item>
        <Grid item container direction="row" spacing={1}>
          {fetchingLoading
            ?
            <Grid container justifyContent="center" alignItems="center">
              <Grid item>
                <CircularProgress/>
              </Grid>
            </Grid>
            :
            albums.map(album => (
              <AlbumItem key={album._id} album={album}/>
            ))
          }
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Albums;