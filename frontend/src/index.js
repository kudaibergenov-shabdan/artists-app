import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Router} from "react-router-dom";
import {Provider} from "react-redux";
import {MuiThemeProvider} from "@material-ui/core";
import history from "./history";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import store from "./store/configureStore";
import theme from "./theme";

const app = (
  <Provider store={store}>
    <Router history={history}>
      <MuiThemeProvider theme={theme}>
        <ToastContainer />
        <App/>
      </MuiThemeProvider>
    </Router>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));