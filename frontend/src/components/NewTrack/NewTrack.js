import React, {useEffect, useState} from 'react';
import {Button, Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsActions";
import {createTrack} from "../../store/actions/tracksActions";
import FormElement from "../UI/Form/FormElement";
import {fetchAlbums} from "../../store/actions/albumsActions";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2),
    },
}));

const NewTrack = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);
    const albums = useSelector(state => state.albums.albums);

    const [state, setState] = useState({
        track: '',
        trackNumber: '',
        duration: '',
        album: '',
        artist: '',
    });

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const onArtistChanged = e => {
        const artist = e.target.value;
        dispatch(fetchAlbums(artist));
    }

    const submitFormHandler = async e => {
        e.preventDefault();
        await dispatch(createTrack({...state}));
    };

    return (
        <>
            <Typography variant="h4">
                New track
            </Typography>
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                className={classes.root}
                autoComplete="off"
                onSubmit={submitFormHandler}
            >
                <FormElement
                    select
                    options={artists}
                    label="Artist"
                    name="artist"
                    value={state.artist}
                    onChange={e => {
                        inputChangeHandler(e);
                        onArtistChanged(e);
                    }}
                />
                <FormElement
                    select
                    options={albums}
                    label="Album"
                    name="album"
                    value={state.album}
                    onChange={inputChangeHandler}
                />
                <FormElement
                    label="Name of track"
                    name="track"
                    value={state.track}
                    onChange={inputChangeHandler}
                />
                <FormElement
                    label="Number of track"
                    name="trackNumber"
                    value={state.trackNumber}
                    onChange={inputChangeHandler}
                />
                <FormElement
                    label="Duration"
                    name="duration"
                    value={state.duration}
                    onChange={inputChangeHandler}
                />
                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">Create</Button>
                </Grid>
            </Grid>
        </>
    );
};

export default NewTrack;