import React from 'react';
import {AppBar, Button, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import UserMenu from "./Menu/UserMenu";

const useStyles = makeStyles(theme => ({
  staticToolbar: {
    marginBottom: theme.spacing(2)
  }
}));

const AppToolbar = () => {
  const classes = useStyles();
  const user = useSelector(state => state.users.user);

  return (
    <>
      <AppBar position="fixed">
        <Toolbar>
          <Grid container justifyContent="space-between" alignContent="center">
            <Grid item>
              <Typography variant="h6">
                SoundTracks
              </Typography>
            </Grid>
            <Grid item>
              {user ? (
                <UserMenu />
              ) : (
                <>
                  <Button component={Link} to="/login" color="inherit">Sign in</Button>
                  <Button component={Link} to="/register" color="inherit">Sign up</Button>
                </>
              )}

            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Toolbar className={classes.staticToolbar} />
    </>
  );
};

export default AppToolbar;