import React, {useState} from 'react';
import {Button, Menu, MenuItem} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {Link} from "react-router-dom";
import {logoutUser} from "../../../../store/actions/usersAction";

const UserMenu = ({user}) => {
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
        My account!
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem>Profile</MenuItem>
        <MenuItem>My account</MenuItem>
        <MenuItem onClick={() => {dispatch(logoutUser())}}>Logout</MenuItem>
      </Menu>
      <Button component={Link} to="/track_history" color="inherit">Visit track history</Button>
    </>
  );
};

export default UserMenu;