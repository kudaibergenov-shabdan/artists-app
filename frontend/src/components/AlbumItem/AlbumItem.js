import React from 'react';
import {
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  IconButton,
  makeStyles,
  Typography
} from "@material-ui/core";
import {Link} from "react-router-dom";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import imageNotAvailable from "../../assets/images/not_available.png";
import {apiURL} from "../../config";

const useStyles = makeStyles(theme => ({
  card: {
    height: '100%',
  },
  media: {
    textAlign: 'center',
  },
  mediaImage: {
    maxWidth: '200px',
    height: 'auto',
  },
}));

const AlbumItem = ({album}) => {
  const classes = useStyles();

  let cardImage = imageNotAvailable;
  if (album.image) {
    const pathToImage = apiURL + '/';
    cardImage = pathToImage + album.image;
  }

  const albumTitle = album.artistInfo[0].title + ' - ' + album.title

  return (
    <Grid item xs={12}>
      <Card className={classes.card}>
        <CardHeader title={albumTitle}/>
        <CardMedia className={classes.media}>
          <img src={cardImage} alt={album.title} className={classes.mediaImage}/>
        </CardMedia>
        {
          (album.issueYear) &&
          (
            <CardContent>
              <Typography variant="subtitle2">
                Year of issue: {album.issueYear}
              </Typography>
            </CardContent>)
        }
        <CardActions>
          <IconButton component={Link} to={`/tracks?album=${album._id}`}>
            <Typography variant="caption">Explore</Typography>
            <ArrowForwardIcon/>
          </IconButton>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default AlbumItem;