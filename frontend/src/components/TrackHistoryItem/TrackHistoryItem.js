import React from 'react';
import {Card, CardContent, CardHeader, Grid, makeStyles, Typography} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  card: {
    height: '100%',
  },
  media: {
    textAlign: 'center',
  },
  mediaImage: {
    maxWidth: '200px',
    height: 'auto',
  },
}));

const TrackHistoryItem = ({histTrack}) => {
  const classes = useStyles();

  return (
    <Grid item xs={12} key={histTrack._id}>
      <Card className={classes.card}>
        <CardHeader title={histTrack.track.track}/>
        <CardContent>
          <Typography variant="subtitle2">
            Album: {histTrack.track.album.title}
          </Typography>
          <Typography variant="subtitle2">
            Listened date: {histTrack.datetime}
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default TrackHistoryItem;