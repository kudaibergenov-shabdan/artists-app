import React from 'react';
import {Card, CardActions, CardHeader, CardMedia, Grid, IconButton, makeStyles, Typography} from "@material-ui/core";
import imageNotAvailable from '../../assets/images/not_available.png';
import {Link} from "react-router-dom";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import {apiURL} from "../../config";

const useStyles = makeStyles(theme => ({
  card: {
    height: '100%',
  },
  media: {
    textAlign: 'center',
  },
  mediaImage: {
    maxWidth: '200px',
    height: 'auto',
  },
}));

const ArtistItem = ({id, title, image, info}) => {
  const classes = useStyles();

  let cardImage = imageNotAvailable;
  if (image) {
    const pathToImage = apiURL + '/';
    cardImage = pathToImage + image;
  }

  return (
    <Grid item xs={12} sm={6} md={6} lg={4}>
      <Card className={classes.card}>
        <CardHeader title={title}/>
        <CardMedia className={classes.media}>
          <img src={cardImage} alt={title} className={classes.mediaImage}/>
        </CardMedia>
        <CardActions>
          <IconButton component={Link} to={`/albums/${id}`}>
            <Typography variant="caption">Explore</Typography>
            <ArrowForwardIcon />
          </IconButton>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default ArtistItem;