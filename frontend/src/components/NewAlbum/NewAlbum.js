import React, {useEffect, useState} from 'react';
import {Button, Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsActions";
import FormElement from "../UI/Form/FormElement";
import {createAlbum} from "../../store/actions/albumsActions";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2),
  },
}));

const NewAlbum = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const artists = useSelector(state => state.artists.artists);

  const [state, setState] = useState({
    title: '',
    artist: '',
    issueYear: null,
    image: '',
  });

  useEffect(() => {
    dispatch(fetchArtists());
  }, [dispatch]);

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];
    setState(prevState => {
      return {...prevState, [name]: file};
    });
  };

  const submitFormHandler = async e => {
    e.preventDefault();

    const formData = new FormData();
    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    await dispatch(createAlbum(formData));
  };

  return (
    <>
      <Typography variant="h4">
        New Album
      </Typography>
      <Grid
        container
        direction="column"
        spacing={2}
        component="form"
        className={classes.root}
        autoComplete="off"
        onSubmit={submitFormHandler}
      >
        <FormElement
          select
          options={artists}
          label="Artist"
          name="artist"
          value={state.artist}
          onChange={inputChangeHandler}
        />
        <FormElement
          label="Title"
          name="title"
          value={state.title}
          onChange={inputChangeHandler}
        />
        <FormElement
          label="Year of issue"
          name="issueYear"
          value={state.issueYear}
          onChange={inputChangeHandler}
        />
        <Grid item xs>
          <TextField
            type="file"
            name="image"
            onChange={fileChangeHandler}
          />
        </Grid>
        <Grid item xs>
          <Button type="submit" color="primary" variant="contained">Create</Button>
        </Grid>
      </Grid>
    </>
  );
};

export default NewAlbum;