import React from 'react';
import {Button, Card, CardActions, CardContent, CardHeader, Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {sendTrack} from "../../store/actions/tracksActions";

const useStyles = makeStyles(theme => ({
  card: {
    height: '100%',
  },
  media: {
    textAlign: 'center',
  },
  mediaImage: {
    maxWidth: '200px',
    height: 'auto',
  },
}));

const TrackItem = ({track}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const addToTrackHistory = track => {
    dispatch(sendTrack(track));
  }

  return (
    <Grid item xs={12}>
      <Card className={classes.card}>
        <CardHeader title={track.track}/>
        <CardContent>
          <Typography variant="subtitle2">
            Sound: {track.track}
          </Typography>
          <Typography variant="subtitle2">
            Number of track: {(track.trackNumber) ? track.trackNumber : 'No info'}
          </Typography>
          <Typography variant="subtitle2">
            Duration: {(track.duration) ? track.duration : 'No info'}
          </Typography>
          <CardActions>
            <Button
              type="button"
              color="primary"
              variant="contained"
              onClick={() => {addToTrackHistory(track)}}
            >
              Add to track history
            </Button>
          </CardActions>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default TrackItem;