import React, {useState} from 'react';
import {Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import FormElement from "../UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {createArtist} from "../../store/actions/artistsActions";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2)
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const NewArtist = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [state, setState] = useState({
    title: '',
    info: '',
    image: null,
  });
  const error = useSelector(state => state.artists.createArtistError);
  const loading = useSelector(state => state.artists.createArtistLoading);

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];
    setState(prevState => {
      return {...prevState, [name]: file};
    });
  };

  const submitFormHandler = async e => {
    e.preventDefault();

    const formData = new FormData();
    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    await dispatch(createArtist(formData));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message
    }
    catch (error) {
      return undefined;
    }
  };

  return (
    <>
      <Typography variant="h4">
        New Artist
      </Typography>
      <Grid
        container
        direction="column"
        spacing={2}
        component="form"
        className={classes.root}
        autoComplete="off"
        onSubmit={submitFormHandler}
        noValidate
      >
        <FormElement
          required
          label="Artist"
          name="title"
          value={state.title}
          onChange={inputChangeHandler}
          error={getFieldError('title')}
        />
        <FormElement
          label="Info"
          name="info"
          value={state.info}
          onChange={inputChangeHandler}
        />
        <Grid item xs>
          <TextField
            type="file"
            name="image"
            onChange={fileChangeHandler}
          />
        </Grid>
        <Grid item xs>
          {/*<Button type="submit" color="primary" variant="contained">Create</Button>*/}
          <ButtonWithProgress
            type="submit"
            variant="contained"
            color="primary"
            className={classes.submit}
            loading={loading}
            disabled={loading}
          >
            Create
          </ButtonWithProgress>
        </Grid>
      </Grid>
    </>
  );
};

export default NewArtist;