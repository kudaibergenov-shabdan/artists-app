import {FETCH_ALBUMS_FAILURE, FETCH_ALBUMS_REQUEST, FETCH_ALBUMS_SUCCESS} from "../actions/albumsActions";

const initialState = {
  albums: [],
  fetchingLoading: false,
};

const albumsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALBUMS_REQUEST:
      return {...state, fetchingLoading: true};
    case FETCH_ALBUMS_SUCCESS:
      return {...state, albums: action.payload, fetchingLoading: false};
    case FETCH_ALBUMS_FAILURE:
      return {...state, fetchingLoading: false};
    default: return {...state};
  }
};

export default albumsReducer;