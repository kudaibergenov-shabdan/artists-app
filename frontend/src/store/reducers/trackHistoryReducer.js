import {
  GET_TRACK_FROM_HISTORY_FAILURE,
  GET_TRACK_FROM_HISTORY_REQUEST,
  GET_TRACK_FROM_HISTORY_SUCCESS
} from "../actions/trackHistoryActions";

const initialState = {
  trackHistory: [],
  fetchingLoading: false,
};

export const trackHistoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TRACK_FROM_HISTORY_REQUEST:
      return {...state, fetchingLoading: true};
    case GET_TRACK_FROM_HISTORY_SUCCESS:
      return {...state, trackHistory: action.payload, fetchingLoading: false};
    case GET_TRACK_FROM_HISTORY_FAILURE:
      return {...state, fetchingLoading: false};
    default:
      return state;
  }
};
