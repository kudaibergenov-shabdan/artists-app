import {
  CREATE_ARTIST_FAILURE, CREATE_ARTIST_REQUEST, CREATE_ARTIST_SUCCESS,
  FETCH_ARTISTS_FAILURE,
  FETCH_ARTISTS_REQUEST,
  FETCH_ARTISTS_SUCCESS
} from "../actions/artistsActions";

const initialState = {
  fetchingLoading: false,
  artists: [],
  createArtistLoading: false,
  createArtistError: null,
};

const artistsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ARTISTS_REQUEST:
      return {...state, fetchingLoading: true};
    case FETCH_ARTISTS_SUCCESS:
      return {...state, artists: action.payload, fetchingLoading: false};
    case FETCH_ARTISTS_FAILURE:
      return {...state, fetchingLoading: false};
    case CREATE_ARTIST_REQUEST:
      return {...state, createArtistLoading: true};
    case CREATE_ARTIST_SUCCESS:
      return {...state, createArtistLoading: false, createArtistError: null};
    case CREATE_ARTIST_FAILURE:
      return {...state, createArtistLoading: false, createArtistError: action.payload};
    default:
      return {...state};
  }
};

export default artistsReducer;