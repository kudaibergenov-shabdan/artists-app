import {FETCH_FAILURE_TRACKS, FETCH_REQUEST_TRACKS, FETCH_SUCCESS_TRACKS} from "../actions/tracksActions";

const initialState = {
  fetchingLoading: false,
  tracks: [],
};

const trackReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_REQUEST_TRACKS:
      return {...state, fetchingLoading: true};
    case FETCH_SUCCESS_TRACKS:
      return {...state, tracks: action.payload, fetchingLoading: false};
    case FETCH_FAILURE_TRACKS:
      return {...state, fetchingLoading: true};
    default:
      return {...state};
  }
};

export default trackReducer;