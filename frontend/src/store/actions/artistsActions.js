import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./historyActions";

export const FETCH_ARTISTS_REQUEST = 'FETCH_ARTISTS_REQUEST';
export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_FAILURE = 'FETCH_ARTISTS_FAILURE';

export const CREATE_ARTIST_REQUEST = 'CREATE_ARTIST_REQUEST';
export const CREATE_ARTIST_SUCCESS = 'CREATE_ARTIST_SUCCESS';
export const CREATE_ARTIST_FAILURE = 'CREATE_ARTIST_FAILURE';

export const fetchArtistsRequest = () => ({type: FETCH_ARTISTS_REQUEST});
export const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, payload: artists});
export const fetchArtistsFailure = () => ({type: FETCH_ARTISTS_FAILURE});

export const createArtistRequest = () => ({type: CREATE_ARTIST_REQUEST});
export const createArtistSuccess = artist => ({type: CREATE_ARTIST_SUCCESS, payload: artist});
export const createArtistFailure = error => ({type: CREATE_ARTIST_FAILURE, payload: error});

export const fetchArtists = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchArtistsRequest());
      const response = await axiosApi.get('/artists');
      dispatch(fetchArtistsSuccess(response.data));
    }
    catch (e) {
      dispatch(fetchArtistsFailure());
    }
  };
};

export const createArtist = artist => {
  return async dispatch => {
    try {
      dispatch(createArtistRequest());
      await axiosApi.post('/artists', artist);
      dispatch(createArtistSuccess(artist));
      dispatch(historyPush('/'));
      toast.success('Artist created');
    }
    catch (error) {
      dispatch(createArtistFailure(error.response.data));
      toast.error('Problem occurred. Could not create artist. Please, try again!');
    }
  };
}