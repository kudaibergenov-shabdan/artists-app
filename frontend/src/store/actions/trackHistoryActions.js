import axiosApi from "../../axiosApi";

export const GET_TRACK_FROM_HISTORY_REQUEST = 'GET_TRACK_FROM_HISTORY_REQUEST';
export const GET_TRACK_FROM_HISTORY_SUCCESS = 'GET_TRACK_FROM_HISTORY_SUCCESS';
export const GET_TRACK_FROM_HISTORY_FAILURE = 'GET_TRACK_FROM_HISTORY_FAILURE';

export const getTrackFromHistoryRequest = () => ({type: GET_TRACK_FROM_HISTORY_REQUEST});
export const getTrackFromHistorySuccess = track => ({type: GET_TRACK_FROM_HISTORY_SUCCESS, payload: track});
export const getTrackFromHistoryFailure = error => ({type: GET_TRACK_FROM_HISTORY_FAILURE, payload: error});

export const fetchTracksHistory = () => {
  return async (dispatch, getState) => {
    try {
      const headers = {
        'Authorization': getState().users.user && getState().users.user.token
      };
      dispatch(getTrackFromHistoryRequest());
      const response = await axiosApi.get('/track_history', {headers});
      dispatch(getTrackFromHistorySuccess(response.data));
    }
    catch (error) {
      if (error.response && error.response.data) {
        dispatch(getTrackFromHistoryFailure(error.response));
      } else {
        dispatch(getTrackFromHistoryFailure({global: 'No Internet'}));
      }
    }
  };
};