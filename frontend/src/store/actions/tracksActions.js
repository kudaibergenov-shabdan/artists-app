import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./historyActions";

export const FETCH_REQUEST_TRACKS = 'FETCH_REQUEST_TRACKS';
export const FETCH_SUCCESS_TRACKS = 'FETCH_SUCCESS_TRACKS';
export const FETCH_FAILURE_TRACKS = 'FETCH_FAILURE_TRACKS';

export const CREATE_REQUEST_TRACK = 'CREATE_REQUEST_TRACK';
export const CREATE_SUCCESS_TRACK = 'CREATE_SUCCESS_TRACK';
export const CREATE_FAILURE_TRACK = 'CREATE_FAILURE_TRACK';

export const getRequestTracks = () => ({type: FETCH_REQUEST_TRACKS});
export const getSuccessTracks = tracks => ({type: FETCH_SUCCESS_TRACKS, payload: tracks});
export const getFailureTracks = () => ({type: FETCH_FAILURE_TRACKS});

export const createRequestTrack = () => ({type: CREATE_REQUEST_TRACK});
export const createSuccessTrack = track => ({type: CREATE_SUCCESS_TRACK, payload: track});
export const createFailureTrack = error => ({type: CREATE_FAILURE_TRACK, payload: error});

export const fetchTracks = albumId => {
  return async dispatch => {
    try {
      dispatch(getRequestTracks());
      const url = `/tracks${albumId}`;
      const response = await axiosApi.get(url);
      dispatch(getSuccessTracks(response.data));
    } catch (e) {
      dispatch(getFailureTracks());
    }
  }
};

export const sendTrack = track => {
  return async (dispatch, getState) => {
    try {
      const headers = {
        'Authorization': getState().users.user && getState().users.user.token
      };
      await axiosApi.post('/track_history', {track}, {headers});
      toast.success(`Track ${track.track} added to history`);
    } catch (error) {
      toast.warning(`Something went wrong`);
    }
  };
};

export const createTrack = track => {
  return async dispatch => {
    try {
      dispatch(createRequestTrack());
      await axiosApi.post('/tracks', track);
      dispatch(createSuccessTrack());
      dispatch(historyPush('/'));
      toast.success('Track created !');
    }
    catch (error) {
      dispatch(createFailureTrack(error));
      toast.error('Problem occurred. Please, try again!')
    }
  };
};