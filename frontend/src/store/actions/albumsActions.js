import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";
import {toast} from "react-toastify";

export const FETCH_ALBUMS_REQUEST = 'FETCH_ALBUMS_REQUEST';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ALBUMS_FAILURE = 'FETCH_ALBUMS_FAILURE';

export const CREATE_ALBUM_REQUEST = 'CREATE_ALBUM_REQUEST';
export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';
export const CREATE_ALBUM_FAILURE = 'CREATE_ALBUM_FAILURE';

export const getAlbumsRequest = () => ({type: FETCH_ALBUMS_REQUEST});
export const getAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, payload: albums});
export const getAlbumsFailure = () => ({type: FETCH_ALBUMS_FAILURE});

export const createAlbumRequest = () => ({type: CREATE_ALBUM_REQUEST});
export const createAlbumSuccess = album => ({type: CREATE_ALBUM_SUCCESS, payload: album});
export const createAlbumFailure = error => ({type: CREATE_ALBUM_FAILURE, payload: error});

export const fetchAlbums = artist => {
  return async dispatch => {
    try {
      dispatch(getAlbumsRequest());
      const response = await axiosApi.get(`/albums/${artist}`);
      dispatch(getAlbumsSuccess(response.data));
    } catch (e) {
      console.log('Error caught');
      dispatch(getAlbumsFailure());
    }
  };
};

export const createAlbum = album => {
  return async dispatch => {
    try {
      dispatch(createAlbumRequest());
      await axiosApi.post('/albums', album);
      dispatch(createAlbumSuccess());
      dispatch(historyPush('/'));
      toast.success('Album created');
    }
    catch (error) {
      dispatch(createAlbumFailure(error));
      toast.error('Problem occurred. Please, try again!');
    }
  }
};