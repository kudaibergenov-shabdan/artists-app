import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import NewArtist from "./components/NewArtist/NewArtist";
import NewAlbum from "./components/NewAlbum/NewAlbum";
import NewTrack from "./components/NewTrack/NewTrack";


const App = () => (
    <Layout>
      <Switch>
        <Route path="/" exact component={Artists}/>
        <Route path="/artist/new" component={NewArtist} />
        <Route path="/albums/:id" component={Albums} />
        <Route path="/album/new" component={NewAlbum} />
        <Route path="/tracks" component={Tracks} />
        <Route path="/track/new" component={NewTrack} />
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login} />
        <Route path="/track_history" component={TrackHistory} />
      </Switch>
    </Layout>
);

export default App;
